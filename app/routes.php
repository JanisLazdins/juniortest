<?php
// Defines method and routes.
$router->get('creator', 'PageController@main');
$router->get('creator/products', 'ProductController@index');
$router->post('creator/products', 'ProductController@inspectFormAction');
$router->get('creator/add', 'AddController@index');
$router->post('creator/store', 'AddController@store');
