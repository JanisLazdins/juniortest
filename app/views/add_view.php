<h2>Insert view</h2>
<h3>Insert product details</h3>
<form method="POST" class="add-form" action="<?php echo $add; ?>">
    <ul>
        <div>
            <label for="sku" class="add-form-label" >SKU:</label>
            <input class="add-form-input" name="sku" value="<?php echo $form->get('sku'); ?>" id="sku"></input>
            <span class="error"><?php echo $form->display('sku'); ?></span>
        </div>
        <div>
            <label for="name" class="add-form-label" >Name:</label>
            <input class="add-form-input" name="name" value="<?php echo $form->get('name'); ?>" id="name"></input>
            <span class="error"><?php echo $form->display('name'); ?></span>
        </div>
        <div>
            <label for="price" class="add-form-label" >Price:</label>
            <input class="add-form-input" name="price" value="<?php echo $form->get('price'); ?>" id="price"></input>
            <span class="error"><?php echo $form->display('price'); ?></span>
        </div>
        <button type="submit" name="addSubmit">Submit</button>
    </ul>
    Choose product type: 
    <select id="type" name="type" onchange="getSelectedType(this);">
            <option>Type Switcher</option>
        <option id="diskOption" value="1">Disk</option>
        <option id="bookOption" value="2">Book</option>
        <option id="furnitureOption" value="3">Furniture</option>
    </select>
    <span><?php echo $form->display('type'); ?></span>
    <span><?php echo $form->inputError(); ?></span>
    <div>
        <div id="1-div" class="hidden" >
            <p class="info">Please​ ​provide​ disk ​size in MB</p>
            <label for="disk_size" class="add-form-label" id="disk-size-lable" >Size: </label>
            <input class="add-form-input" name="disk_size" id="disk_size" ></input>
            <span><?php echo $form->display('disk_size'); ?></span>
        </div>
        <div id="2-div" class="hidden">
            <p class="info">Please​ ​provide​ disk ​size in KG</p>
            <label for="book_weight" class="add-form-label" id="book-weight-lable">Weight:</label>
            <input class="add-form-input" name="book_weight" id="book_weight"></input>
            <span><?php echo $form->display('book_weight'); ?></span>
        </div>
        <div id="3-div" class="hidden">
            <p class="info">Please​ ​provide​ ​dimensions​ ​in​ ​HxWxL​ ​format</p>
            <div class="mutiple-input">
                <label for="furniture_height" class="add-form-label" id="furniture-height-lable" >Height: </label>
                <input class="add-form-input" name="furniture_height" id="furniture_height"></input>
                <span><?php echo $form->display('furniture_height'); ?></span>
            </div>
            <div class="mutiple-input">
                <label for="urniture_width" class="add-form-label" id="furniture-width-lable">Width: </label>
                <input class="add-form-input" name="furniture_width" id="furniture_width"></input>
                <span><?php echo $form->display('furniture_width'); ?></span>
            </div>
            <div class="mutiple-input">
                <label for="furniture_lenght" class="add-form-label" id="furniture-lenght-lable">Length: </label>
                <input class="add-form-input" name="furniture_lenght" id="furniture_lenght"></input>
                <span><?php echo $form->display('furniture_lenght'); ?></span>
            </div>
        </div>
    </div>
</form>
