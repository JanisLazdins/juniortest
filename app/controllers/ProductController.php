<?php

namespace App\Controllers;

/**
 * This class is responsible for Inspect view and showing the products.
 */
class ProductController extends Controller
{

    /**
     * Variable $products Contains all retrieved data from MySql already sorted by type.
     *
     * @return string Loads 'inspect' view and sends $products variable to it.
     */
    public function index()
    {
        $products = $this->model->productListings();
        return $this->helper->view('products', compact('products'));
    }

    /**
     * This method is executed if form in the 'inspect' view has been submitted.
     * Action depends on the chosen option.
     * Redirects to QueryBuilder class and passes table name and
     * Return view 'products'.
     *
     * @return string
     */
    public function inspectFormAction()
    {
        $action = $_POST['delete'];
        $checkbox = $_POST['checkbox'];
        if (isset($_POST['submit2'])) {
            if ($action == 'mass-delete' && !empty($checkbox)) {
                $this->model->massDelete($checkbox);
            }
        }
        return $this->helper->redirecting('products');
    }
}