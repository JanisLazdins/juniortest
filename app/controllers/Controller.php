<?php

namespace App\Controllers;

use App\Models\ProductModel;


/**
 * Main controller.
 */
class Controller
{

    /**
     *  Initialize classes
     */
    function __construct()
    {
        $this->model = new ProductModel();
        $this->helper = new \App\Controllers\Helpers();
    }
}