/*
 Receives 'select' tags element.
 Displays div elements which are associated with the chosen option.
 
 $param object selectedType
 */
function getSelectedType(selectedType)
{
    diskOptionValue = document.getElementById("diskOption").value;
    bookOptionValue = document.getElementById("bookOption").value;
    furnitureOptionValue = document.getElementById("furnitureOption").value;

    if (diskOptionValue == selectedType.value) {
        document.getElementById("1-div").style.display = "inline-block";
    } else {
        document.getElementById("1-div").style.display = "none";
    }

    if (bookOptionValue == selectedType.value) {
        document.getElementById("2-div").style.display = "inline-block";
    } else {
        document.getElementById("2-div").style.display = "none";
    }

    if (furnitureOptionValue == selectedType.value) {
        document.getElementById("3-div").style.display = "inline-block";
    } else {
        document.getElementById("3-div").style.display = "none";
    }
}