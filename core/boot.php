<?php

use App\Core\Container;

/**
 * Defines root directory for the project.
 */
define('URL', 'http://localhost/creator/');
/**
 * Assigns key "config" to file config.php contents.
 */
Container::assign('config', require 'config.php');

/**
 * Retrieves content associated with keys "config" and "database".
 * Assigns a class to key "database".
 * Data of "config" and "database" is used to make a connection to the database.
 * Connection data is used in QueryBuilder instance.
 */
Container::assign('database', new QueryBuilder(
    Connection::Connect(Container::retrieve('config')['database'])
));