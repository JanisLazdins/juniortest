<?php

namespace App\Core;

class Container
{
    /**
     * All registered keys.
     *
     * @var array
     */
    protected static $keys = [];

    /**
     * Assigns a key to a value in the container.
     *
     * @param sting $key Assigns keys to and array data
     *
     * @param string $value Assigns properties to array data associated with the key
     */
    public static function assign($key, $value)
    {
        static::$keys[$key] = $value;
    }

    /**
     * Retrieves the array.
     * If key does not exist Exception is thrown.
     * 
     * @param string $key
     * @throws Exception
     */
    public static function retrieve($key)
    {
        if (array_key_exists($key, static::$keys)) {
            return static::$keys[$key];
        }

        throw new Exception("No {$key} is bound in the container");
    }
}