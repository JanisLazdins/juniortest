<?php

/**
 * Make a connection to database.
 *
 * Creates connection to database with dynamically inserted variables.
 */
class Connection
{

    /**
     * @param array $config Inserted MySql login data from config.php.
     * @return \PDO
     */
    public static function Connect($config)
    {
        try {
            return new PDO(
                $config['connection'].';dbname='.$config['name'],
                $config['username'], $config['password'], $config['options']
            );
        } catch (PDOException $e) {
            die('Could not connect');
        }
    }
}